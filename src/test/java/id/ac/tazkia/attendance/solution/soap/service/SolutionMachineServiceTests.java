package id.ac.tazkia.attendance.solution.soap.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.attendance.solution.soap.dto.GetAttendanceRequest;
import id.ac.tazkia.attendance.solution.soap.dto.GetAttendanceResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SolutionMachineServiceTests {
    @Autowired private ObjectMapper objectMapper;
    @Autowired private SolutionMachineService service;

    @Test
    public void testGetAttendance() throws Exception {
        GetAttendanceRequest request = new GetAttendanceRequest();
        request.setArgComKey(123);
        request.getArg().put("PIN", "214");

        String ipAddress = "192.168.44.25";
        //String ipAddress = "127.0.0.1:12345";

        GetAttendanceResponse response
                = service.getAttendance(ipAddress, request);

        for (GetAttendanceResponse.Row r : response.getRows()) {
            System.out.println("ID Pegawai : "+r.getPin());
            System.out.println("Jam Tap : "+r.getDateTime());
        }
    }
}
