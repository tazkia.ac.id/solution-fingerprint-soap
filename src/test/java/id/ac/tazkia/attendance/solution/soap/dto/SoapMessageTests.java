package id.ac.tazkia.attendance.solution.soap.dto;

import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.util.stream.Collectors;

@SpringBootTest
public class SoapMessageTests {
    @Value("classpath:/xml/get-attendance-response.xml")
    private Resource getAttendanceResponseSample;

    @Autowired
    private XmlMapper xmlMapper;

    @Test
    public void testGenerateGetAttendanceRequest() throws Exception {
        GetAttendanceRequest request = new GetAttendanceRequest();
        request.setArgComKey(1234);
        request.getArg().put("PIN", "9999");

        String xmlRequest = xmlMapper.writeValueAsString(request);
        Assertions.assertNotNull(xmlRequest);
        System.out.println(xmlRequest);
    }

    @Test
    public void testGenerateGetAttendanceResponse() throws Exception {
        GetAttendanceResponse response = new GetAttendanceResponse();
        GetAttendanceResponse.Row row1 = new GetAttendanceResponse.Row();
        row1.setDateTime(LocalDateTime.now().minusDays(1));
        row1.setPin("1234");
        row1.setStatus("Status");
        row1.setVerified("X");
        row1.setWorkCode("Code1234");
        response.getRows().add(row1);

        GetAttendanceResponse.Row row2 = new GetAttendanceResponse.Row();
        row2.setDateTime(LocalDateTime.now());
        row2.setPin("1234");
        row2.setStatus("Status");
        row2.setVerified("X");
        row2.setWorkCode("Code1234");
        response.getRows().add(row2);
        String xmlResponse = xmlMapper.writeValueAsString(response);
        System.out.println(xmlResponse);
    }

    @Test
    public void testReadGetAttendanceResponse() throws Exception {

        String strResponse = new BufferedReader(
                new InputStreamReader(getAttendanceResponseSample.getInputStream()))
                .lines().collect(Collectors.joining());

        GetAttendanceResponse response = xmlMapper
                .readValue(strResponse, GetAttendanceResponse.class);
        Assertions.assertNotNull(response);
        System.out.println(response);

    }
}