package id.ac.tazkia.attendance.solution.soap.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data @JacksonXmlRootElement(localName = "GetAttLogResponse")
public class GetAttendanceResponse {

    @JacksonXmlProperty(localName = "Row")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<Row> rows = new ArrayList<>();

    @Data
    public static class Row{

        @JacksonXmlProperty(localName = "PIN")
        private String pin;

        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        @JacksonXmlProperty(localName = "DateTime")
        private LocalDateTime dateTime;

        @JacksonXmlProperty(localName = "Verified")
        private String verified;

        @JacksonXmlProperty(localName = "Status")
        private String status;

        @JacksonXmlProperty(localName = "WorkCode")
        private String workCode;
    }
}
