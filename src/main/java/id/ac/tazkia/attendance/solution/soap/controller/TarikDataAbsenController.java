package id.ac.tazkia.attendance.solution.soap.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.tazkia.attendance.solution.soap.dto.GetAttendanceRequest;
import id.ac.tazkia.attendance.solution.soap.dto.GetAttendanceResponse;
import id.ac.tazkia.attendance.solution.soap.dto.TarikAbsensiDto;
import id.ac.tazkia.attendance.solution.soap.service.SolutionMachineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.util.ArrayList;
import java.util.List;

@Controller
public class TarikDataAbsenController {
    @Autowired private SolutionMachineService service;

    @GetMapping( "/api/tarikabsensi/pegawai")
    @ResponseBody
    public ResponseEntity<List<TarikAbsensiDto>> absensiKaryawan() {

        GetAttendanceRequest request = new GetAttendanceRequest();
        request.setArgComKey(123);
        request.getArg().put("PIN", "214");
        String ipAddress = "192.168.44.25";

        GetAttendanceResponse response
                = service.getAttendance(ipAddress, request);

        List<TarikAbsensiDto> tarikAbsensiDtos = new ArrayList<>();

        for (GetAttendanceResponse.Row r : response.getRows()) {
            TarikAbsensiDto tarikAbsensiDto = new TarikAbsensiDto();
            tarikAbsensiDto.setId(r.getPin());
            tarikAbsensiDto.setTanggal(r.getDateTime().toLocalDate());
            tarikAbsensiDto.setWaktu(r.getDateTime().toLocalTime());

            tarikAbsensiDtos.add(tarikAbsensiDto);
        }

        final HttpHeaders httpHeaders= new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        return new ResponseEntity<>(tarikAbsensiDtos, httpHeaders, HttpStatus.OK);

    }

}
