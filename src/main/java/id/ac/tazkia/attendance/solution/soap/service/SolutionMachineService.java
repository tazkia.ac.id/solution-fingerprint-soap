package id.ac.tazkia.attendance.solution.soap.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import id.ac.tazkia.attendance.solution.soap.dto.GetAttendanceRequest;
import id.ac.tazkia.attendance.solution.soap.dto.GetAttendanceResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Service @Slf4j
public class SolutionMachineService {
    private static final String URL = "/iWsService";

    @Autowired private XmlMapper xmlMapper;

    public GetAttendanceResponse getAttendance(String machineIp, GetAttendanceRequest request)  {
        try {
            return WebClient.create("http://"+machineIp)
                    .post().uri(URL)
                    .contentType(MediaType.TEXT_XML)
                    .bodyValue(xmlMapper.writeValueAsString(request))
                    .retrieve()
                    .bodyToMono(String.class)
                    .flatMap(s-> Mono.fromCallable(() -> xmlMapper.readValue(s, GetAttendanceResponse.class)))
                    .block();
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
