package id.ac.tazkia.attendance.solution.soap.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data @JacksonXmlRootElement(localName = "GetAttLog")
public class GetAttendanceRequest {

    @JacksonXmlProperty(localName = "ArgComKey")
    private Integer argComKey;

    @JacksonXmlProperty(localName = "Arg")
    private Map<String, String> arg = new HashMap<>();
}
