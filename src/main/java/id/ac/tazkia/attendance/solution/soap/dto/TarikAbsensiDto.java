package id.ac.tazkia.attendance.solution.soap.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TarikAbsensiDto {

    private String id;

    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate tanggal;

    @JsonFormat(pattern = "HH:mm:ss")
    private LocalTime waktu;

}
