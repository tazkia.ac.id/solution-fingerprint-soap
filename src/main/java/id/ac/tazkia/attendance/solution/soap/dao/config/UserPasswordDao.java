package id.ac.tazkia.attendance.solution.soap.dao.config;

import id.ac.tazkia.attendance.solution.soap.entity.config.User;
import id.ac.tazkia.attendance.solution.soap.entity.config.UserPassword;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserPasswordDao extends PagingAndSortingRepository<UserPassword, String> {
    UserPassword findByUser(User user);
}
