package id.ac.tazkia.attendance.solution.soap.dao.config;

import id.ac.tazkia.attendance.solution.soap.entity.config.Role;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RoleDao extends PagingAndSortingRepository<Role, String> {


}
