package id.ac.tazkia.attendance.solution.soap.dao.config;

import id.ac.tazkia.attendance.solution.soap.entity.config.User;
import id.ac.tazkia.attendance.solution.soap.entity.config.User;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserDao extends PagingAndSortingRepository<User, String> {
    User findByUsername(String username);
}
