package id.ac.tazkia.attendance.solution.soap.entity.config;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "s_user")
//@Data
@Getter
@Setter
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(generator = "uuid" )
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    private String username;
    private Boolean active;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "id_role")
    private Role role;


}
